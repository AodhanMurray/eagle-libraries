# Eagle Libraries
### Summary 
A collection of eagle libaries containg symbols and footprints developed and used through projects. 

### Devices
#### atmel-custom.lbr
- ATSAMD10D14A-MNT

#### clock-custom.lbr
- CRYSTAL
- OSCILLATOR
- SG5032CAN
- 74AVC9112GTX

#### comm-custom.lbr
- SI8641BA

#### conn-custom.lbr
- PT1,5/10-5.0-H
- SDCARD
- TESTPOINT
- 1990164
- PINHD-1X18
- 1X18
- PINHD-1X6
- PINHD-1X3
- PINHD-2X6

#### measurement-custom.lbr
- ADE9000
- ADE7953
- ADS131E08
- MAX6072
- LTC6655

#### micro-custom.lbr
- ATSAME54N20A

#### opamp-custom.lbr
- TLV433
- TLV333

#### passives-custom.lbr
- NPIM104P
- UWT1C
- NPIS25H

#### power-custom.lbr
- LM2664
- MIC5271YM5
- TMA0505D
- TC1014
- LP2980
- XRP6124
- ROE-0505S

#### sensor-custom.lbr
- LTS15-NP
- LV-25-P
- BPW34

#### trans-custom.lbr
- DMP3028LFDE

#### transfo-custom.lbr
- 44193
- TV19
